<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('barang', BarangController::class);
Route::get('/barang', 'BarangController@index')->name('barang');
Route::get('/barang/create', 'BarangController@create')->name('barang.create');
Route::PUT('/barang/store', 'BarangController@store')->name('barang.store');
Route::get('/barang/edit/{id}', 'BarangController@edit')->name('barang.edit');
Route::PUT('/barang/update/{id}', 'BarangController@update')->name('barang.update');
Route::get('/barang/hapus/{id}', 'BarangController@destroy')->name('barang.hapus');

Route::get('/pelanggan', 'PelangganController@index')->name('pelanggan');
Route::get('/pelanggan/create', 'PelangganController@create')->name('pelanggan.create');
Route::PUT('/pelanggan/store', 'PelangganController@store')->name('pelanggan.store');
Route::get('/pelanggan/edit/{id}', 'PelangganController@edit')->name('pelanggan.edit');
Route::PUT('/pelanggan/update/{id}', 'PelangganController@update')->name('pelanggan.update');
Route::get('/pelanggan/hapus/{id}', 'PelangganController@destroy')->name('pelanggan.hapus');

Route::get('/supplier', 'SupplierController@index')->name('supplier');
Route::get('/supplier/create', 'SupplierController@create')->name('supplier.create');
Route::PUT('/supplier/store', 'SupplierController@store')->name('supplier.store');
Route::get('/supplier/edit/{id}', 'SupplierController@edit')->name('supplier.edit');
Route::PUT('/supplier/update/{id}', 'SupplierController@update')->name('supplier.update');
Route::get('/supplier/hapus/{id}', 'SupplierController@destroy')->name('supplier.hapus');

Route::get('/penjualan', 'PenjualanController@index')->name('penjualan');
Route::get('/penjualan/create', 'PenjualanController@create')->name('penjualan.create');
Route::PUT('/penjualan/store', 'PenjualanController@store')->name('penjualan.store');
// Route::get('/penjualan/edit/{id}', 'PenjualanController@edit')->name('penjualan.edit');
// Route::PUT('/penjualan/update/{id}', 'PenjualanController@update')->name('penjualan.update');
// Route::get('/penjualan/hapus/{id}', 'penjualanController@destroy')->name('penjualan.hapus');

Route::get('/pembelian', 'PembelianController@index')->name('pembelian');
Route::get('/pembelian/create', 'PembelianController@create')->name('pembelian.create');
Route::PUT('/pembelian/store', 'PembelianController@store')->name('pembelian.store');
// Route::get('/pembelian/edit/{id}', 'PembelianController@edit')->name('pembelian.edit');
// Route::PUT('/pembelain/update/{id}', 'PembelianController@update')->name('pembelian.update');
// Route::get('/pembelian/hapus/{id}', 'pembelianController@destroy')->name('pembelian.hapus');

