<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'Barang';
    protected $fillable = ['kode_barang', 'nama_barang', 'stok_barang', 'harga_barang'];
    protected $guarded =[];

    public function penjualan(){
        return $this->hasMany(Penjualan::class);
    }
    public function pembelian(){
        return $this->hasMany(Pembelian::class);
    }
}
