<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Penjualan;
use App\Barang;
use App\Pelanggan;


class PenjualanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_penjualan = Penjualan::all();
        
        // return $data_penjualan;
        // $data_penjualan = DB::table('penjualan')
        //                 ->join('barang', 'penjualan.id_barang', '=', 'barang.id_barang')
        //                 ->join('pelanggan', 'penjualan.id_pelanggan', '=', 'pelanggan.id_pelanggan')
        //                 ->select('penjualan.*', 'barang.*', 'pelanggan.*')
        //                 // ->where('status','=', '0')
        //                 ->get();

        
        // $data = array(
        //     'pelanggan'  => $data_pelanggan,
        //     'barang'   => $data_barang,
        // );
        // $data_barang = DB::table('barang')->get();
        // 
        return view('penjualan', compact('data_penjualan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_pelanggan= Pelanggan::all();
        $data_barang  = Barang::all();
        return view('penjualan_create', compact('data_penjualan','data_barang','data_pelanggan'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'no_penjualan' => 'required|unique:penjualan,no_penjualan',
        ]);
        
        Penjualan::create($request->all());
        return redirect()->route('penjualan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
