<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_supplier = Supplier::all();
        // $data_barang = DB::table('barang')->get();
        // 
        return view('supplier', compact('data_supplier'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supplier_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_supplier' => 'required|unique:supplier,kode_supplier',
        ]);
        Supplier::create($request->all());
        return redirect()->route('supplier');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_supplier=Supplier::where('id', $id)->firstorfail();
        
        return view('supplier_edit', compact('data_supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_supplier=Supplier::where('id',$id)->update([
            'kode_supplier'=>$request['kode_supplier'],
            'nama_supplier'=>$request['nama_supplier'],
            'alamat'=>$request['alamat'],
            'no_telepon'=>$request['no_telepon'],
        ]);
        return redirect()->route('supplier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_supplier=Supplier::where('id',$id)->delete();
        return redirect()->route('supplier');
    }
}
