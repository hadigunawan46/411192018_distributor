<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Barang;
use Illuminate\Http\Request;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_barang = Barang::all();
        // $data_barang = DB::table('barang')->get();
        // 
        return view('barang', compact('data_barang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // \App\Barang::create($request->all());
        return view('barang_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_barang' => 'required|unique:barang,kode_barang',
            'nama_barang' => 'required',
        ]);
        Barang::create($request->all());
        return redirect()->route('barang');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_barang=Barang::where('id', $id)->firstorfail();
        
        return view('barang_edit', compact('data_barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $barang=Barang::where('id',$id)->update([
            'kode_barang'=>$request['kode_barang'],
            'nama_barang'=>$request['nama_barang'],
            'stok_barang'=>$request['stok_barang'],
            'harga_barang'=>$request['harga_barang'],
        ]);
        return redirect()->route('barang');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_barang=Barang::where('id',$id)->delete();
        return redirect()->route('barang');
    }
}
