<?php

namespace App\Http\Controllers;

use App\Pelanggan;
use Illuminate\Http\Request;

class PelangganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_pelanggan = Pelanggan::all();
        // $data_barang = DB::table('barang')->get();
        // 
        return view('pelanggan', compact('data_pelanggan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pelanggan_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'kode_pelanggan' => 'required|unique:pelanggan,kode_pelanggan',
        ]);
        Pelanggan::create($request->all());
        return redirect()->route('pelanggan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_pelanggan=Pelanggan::where('id', $id)->firstorfail();
        
        return view('pelanggan_edit', compact('data_pelanggan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_pelanggan=Pelanggan::where('id',$id)->update([
            'kode_pelanggan'=>$request['kode_pelanggan'],
            'nama_pelanggan'=>$request['nama_pelanggan'],
            'alamat'=>$request['alamat'],
            'no_telepon'=>$request['no_telepon'],
        ]);
        return redirect()->route('pelanggan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_pelanggan=Pelanggan::where('id',$id)->delete();
        return redirect()->route('pelanggan');
    }
}
