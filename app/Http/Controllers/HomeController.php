<?php

namespace App\Http\Controllers;

use App\Barang;
use App\Pembelian;
use App\Penjualan;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_barang = Barang::count();
        $total_penjualan = Penjualan::count();
        $total_pembelian = Pembelian::count();

        return view('home', compact('total_barang','total_penjualan', 'total_pembelian'));
    }
}
