<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penjualan extends Model
{
    protected $primaryKey ='id';
    protected $table = 'penjualan';
    protected $fillable = ['no_penjualan','tanggal','id_pelanggan','id_barang', 'jumlah_barang', 'harga_barang','nama_pelanggan','nama_barang'];
    // protected $guarded =[];

    public function pelanggan(){
        return $this->belongsTo('App\Pelanggan');
    }
    
    public function barang(){
        return $this->belongsTo('App\Barang');
    }
}
