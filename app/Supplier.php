<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    protected $table = 'supplier';
    protected $fillable = ['kode_supplier', 'nama_supplier', 'alamat', 'no_telepon'];
    protected $guarded =[];

    public function pembelian(){
        return $this->hasMany(Pembelian::class);
    }
}
