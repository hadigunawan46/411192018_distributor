<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pelanggan extends Model
{
    protected $table = 'pelanggan';
    protected $fillable = ['kode_pelanggan', 'nama_pelanggan', 'alamat', 'no_telepon'];
    protected $guarded =[];

    public function penjualan(){
        return $this->hasMany(Penjualan::class);
    }
}
