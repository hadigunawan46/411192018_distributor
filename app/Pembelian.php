<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pembelian extends Model
{
    protected $table = 'pembelian';
    protected $fillable = ['no_pembelian','tanggal','id_supplier','id_barang', 'jumlah_barang', 'harga_barang',];
    protected $guarded =[];
    
    public function supplier(){
        return $this->belongsTo('App\Supplier');
    }
    
    public function barang(){
        return $this->belongsTo('App\Barang');
    }   
}
