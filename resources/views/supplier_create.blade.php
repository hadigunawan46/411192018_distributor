@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">
                        <h1>Tambah Data Supplier</h1>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('supplier.store') }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Kode Supplier</label>
                                <div class="col-md-6">
                                    <input type="text" name="kode_supplier"
                                        class="form-control @error('kode_supplier') is-invalid @enderror">
                                    @error('kode_supplier')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Nama Supplier</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_supplier" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Alamat</label>
                                <div class="col-md-6">
                                    <textarea name="alamat" id="" cols="40" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">No Telepon</label>
                                <div class="col-md-6">
                                    <input type="text" name="no_telepon" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right"></label>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
