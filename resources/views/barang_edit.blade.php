@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">
                        <h1>Edit Data Barang</h1>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('barang.update', $data_barang['id']) }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Kode Barang</label>
                                <div class="col-md-6">
                                    <input type="text" name="kode_barang" value="{{ $data_barang['kode_barang'] }}"
                                        class="form-control @error('kode_barang') is-invalid @enderror">
                                    @error('kode_barang')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Nama Barang</label>
                                <div class="col-md-6">
                                    <input type="text" name="nama_barang"
                                        value="{{ $data_barang['nama_barang'] }}"class="form-control ">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Stok Barang</label>
                                <div class="col-md-6">
                                    <input type="number" name="stok_barang"
                                        value="{{ $data_barang['stok_barang'] }}"class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Harga Barang</label>
                                <div class="col-md-6">
                                    <input type="number" name="harga_barang"
                                        value="{{ $data_barang['harga_barang'] }}"class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right"></label>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" type="submit">Edit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
