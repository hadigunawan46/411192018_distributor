@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header"><h1>Data Pembelian</h1></div>

                <div class="card-body">
                    <a href="{{ route('pembelian.create') }}" class="btn btn-primary mb-3">Tambah Data</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Pembelian</th>
                                <th>Tanggal</th>
                                <th>Id Supplier</th>
                                <th>Id Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Harga Barang</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_pembelian as $pembelian)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $pembelian->no_pembelian }}</td>
                                    <td>{{ $pembelian->tanggal }}</td>
                                    <td>{{ $pembelian->id_supplier }}</td>
                                    <td>{{ $pembelian->id_barang }}</td>
                                    <td>{{ $pembelian->jumlah_barang }}</td>
                                    <td>{{ $pembelian->harga_barang }}</td>
                                    {{-- <td>
                                        <a href="{{ route('pelanggan.edit', $pelanggan['id']) }}" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="{{ route('pelanggan.hapus', $pelanggan['id']) }}" class="btn btn-danger btn-sm">Hapus</a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

