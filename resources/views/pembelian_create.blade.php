@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">
                        <h1>Tambah Data Pembelian</h1>
                    </div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('pembelian.store') }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">No Pembelian</label>
                                <div class="col-md-6">
                                    <input type="text" name="no_pembelian"
                                        class="form-control @error('no_pembelian') is-invalid @enderror">
                                    @error('no_pembelian')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Tanggal</label>
                                <div class="col-md-6">
                                    <input type="date" name="tanggal" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Nama Supplier</label>
                                <select class="form-control" name="id_pelanggan">
                                    <option>- Nama Supplier -</option>
                                    @foreach($data_supplier as $supplier)
                                    <option value="{{$supplier->kode_supplier}}">{{$supplier->nama_supplier}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Nama Barang</label>
                                <select class="form-control" name="id_barang">
                                    <option>- Nama barang -</option>
                                    @foreach($data_barang as $barang)
                                    <option value="{{$barang->kode_barang}}">{{$barang->nama_barang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Jumlah Barang</label>
                                <div class="col-md-6">
                                    <input type="number" name="jumlah_barang" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 text-md-right">Harga Barang</label>
                                <div class="col-md-6">
                                    <input type="number" name="harga_barang" class="form-control">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 text-md-right"></label>
                                <div class="col-md-6">
                                    <button class="btn btn-primary" type="submit">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
