@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header"><h1>Data Barang</h1></div>

                <div class="card-body">
                    <a href="{{ route('barang.create') }}" class="btn btn-primary mb-3">Tambah Data</a>
                    <table class="table table-striped ">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Barang</th>
                                <th>Nama Barang</th>
                                <th>Stok Barang</th>
                                <th>Harga Barang</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_barang as $barang)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $barang->kode_barang }}</td>
                                    <td>{{ $barang->nama_barang }}</td>
                                    <td>{{ $barang->stok_barang }}</td>
                                    <td>{{ $barang->harga_barang }}</td>
                                    <td>
                                        <a href="{{ route('barang.edit', $barang['id']) }}" class="btn btn-warning btn-sm ">Edit</a>
                                        <a href="{{ route('barang.hapus', $barang['id']) }}" class="btn btn-danger btn-sm">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

