@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header"><h1>Data Penjualan</h1></div>

                <div class="card-body">
                    <a href="{{ route('penjualan.create') }}" class="btn btn-primary mb-3">Tambah Data</a>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Penjualan</th>
                                <th>Tanggal</th>
                                <th>Nama Pelanggan</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Barang</th>
                                <th>Harga Barang</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data_penjualan as $penjualan)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $penjualan->no_penjualan }}</td>
                                    <td>{{ $penjualan->tanggal }}</td>
                                    <td>{{ $penjualan->id_pelanggan }}</td>
                                    <td>{{ $penjualan->id_barang }}</td>
                                    <td>{{ $penjualan->jumlah_barang }}</td>
                                    <td>{{ $penjualan->harga_barang }}</td>
                                    {{-- <td>
                                        <a href="{{ route('pelanggan.edit', $pelanggan['id']) }}" class="btn btn-warning btn-sm">Edit</a>
                                        <a href="{{ route('pelanggan.hapus', $pelanggan['id']) }}" class="btn btn-danger btn-sm">Hapus</a>
                                    </td> --}}
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

