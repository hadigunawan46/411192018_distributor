@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                
                <div class="card-header"><h1>Edit Data Supplier</h1></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('supplier.update',$data_supplier['id']) }}" >
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label class="col-md-4 text-md-right">Kode Supplier</label>
                            <div class="col-md-6">
                                <input type="text" name="kode_supplier" value="{{ $data_supplier['kode_supplier'] }}" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 text-md-right">Nama Barang</label>
                            <div class="col-md-6">
                                <input type="text" name="nama_supplier" value="{{ $data_supplier['nama_supplier'] }}"class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 text-md-right">Alamat</label>
                            <div class="col-md-6">
                                <textarea name="alamat" id="" cols="40" rows="3" value="{{ $data_supplier['alamat'] }}"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 text-md-right">NO Telepon</label>
                            <div class="col-md-6">
                                <input type="number" name="no_telepon" value="{{ $data_supplier['no_telepon'] }}"class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 text-md-right"></label>
                            <div class="col-md-6">
                                <button class="btn btn-primary" type="submit">Edit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

